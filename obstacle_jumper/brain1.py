from pybrain.tools.shortcuts import buildNetwork

layers = 4
vecsize = 10
net = buildNetwork(vecsize, layers, 2)
#net.activate([10]*40)

from pybrain.datasets import SupervisedDataSet

ds = SupervisedDataSet(vecsize, 2)

f=open("dumps/dump1.txt","r")
for line in f:
	line=line.strip()
	input=line[0:vecsize]
	input=[int(x) for x in list(input)]
	output=line[40:41]
	if output=="0":
		output=[0,1]
	elif output=="1":
		output=[1,0]
	print tuple(input),tuple(output)
	ds.addSample(tuple(input), tuple(output))

from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import TanhLayer
net = buildNetwork(vecsize, layers, 2, bias=True, hiddenclass=TanhLayer)

trainer = BackpropTrainer(net, ds)
trainer.train()
#trainer.trainUntilConvergence()
trainer.trainEpochs(epochs=100)
#print net.activate((0,0,3,0))

import pickle
f=open("brain_1","w")
pickle.dump(net, f)
f.close()
