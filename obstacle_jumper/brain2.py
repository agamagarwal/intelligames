from pybrain.tools.shortcuts import buildNetwork

layers = 4
net = buildNetwork(6, layers, 2)
#net.activate([10]*40)

from pybrain.datasets import SupervisedDataSet

ds = SupervisedDataSet(6, 2)

f=open("dump.txt","r")
for line in f:
	line=line.strip()
	input=line[0:6]
	input=[int(x) for x in list(input)]
	output=line[40:41]
	if output=="00":
		output=[0,0,0,1]
	elif output=="01":
		output=[0,0,1,0]
	elif output=="10":
		output=[0,1,0,0]
	elif output=="11":
		output=[1,0,0,0]
	print tuple(input),tuple(output)
	ds.addSample(tuple(input), tuple(output))

from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import TanhLayer
net = buildNetwork(6, layers, 4, bias=True, hiddenclass=TanhLayer)

trainer = BackpropTrainer(net, ds)
trainer.train()
#trainer.trainUntilConvergence()
trainer.trainEpochs(epochs=100)
#print net.activate((0,0,3,0))

import pickle
f=open("brain_1","w")
pickle.dump(net, f)
f.close()
