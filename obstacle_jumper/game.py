import ConfigParser
import os
import threading
import time
import sys
import termios
import tty
from gamesupport import _find_getch
import random

getch=_find_getch()


cfg_file = 'game.cfg'

cfg = ConfigParser.ConfigParser()
cfg.read(cfg_file)

try:
	jump_height = int(cfg.get('player', 'jump_height'))
	game_width = int(cfg.get('game', 'width'))
	game_height = int(cfg.get('game', 'height'))
except ConfigParser.NoOptionError:
	print "Invalid configuration"
	exit()

print game_height, game_width
print jump_height
f=open("dump.txt","w")
class game:
	def __init__(self):
		self.frame=[[0 for i in range(0, game_width)] for j in range(0, game_height)]
		for i in range(0,game_width):
			self.frame[game_height-1][i]=1
		self.frame[game_height-2][2]=3
		self.fkey=0
		self.jumpkey=0
		self.height=0
		self.velocity=0
		self.runningThread=0
		self.commandkill=0
	def getFrame(self):
		return self.frame

	def pressKeyUp(self):
		self.jumpkey=1
	def pressKeyForward(self):
		self.fkey=1
	def processFrame(self):
		global jump_height
		self.fkey=1
		temp_frame=[[0 for i in range(0, game_width)] for j in range(0, game_height)]
		for i in range(0, game_height):
			for j in range(0,game_width-1):
				if self.fkey:
					temp_frame[i][j]=self.frame[i][j+1]
				else:
					temp_frame[i][j]=self.frame[i][j]
				if temp_frame[i][j] == 3:
					temp_frame[i][j] = 0

		
		if self.jumpkey == 1 and self.height==0:
			self.velocity=1
		self.jumpkey = 0

		self.height+=self.velocity
	
		if self.height >= jump_height:
			self.velocity=-1
			
		if self.velocity==-1 and self.height==0:
			self.velocity=0
		if temp_frame[game_height-2-self.height][2]==2:
			self.commandkill=1
			temp_frame[game_height-2-self.height][2]=4
		else:
			temp_frame[game_height-2-self.height][2]=3
		if self.fkey:
			temp_frame[game_height-1][game_width-1]=1
			chance=random.randint(0,9)
			if chance == 0:
				temp_frame[game_height-2][game_width-1]=2
		else:
			temp_frame[game_height-1][game_width-1]=self.frame[game_height-1][game_width-1]

				
		self.fkey=0
		
		self.frame=temp_frame

	def processor(self, callback,n):
		while 1:
			str1=self.getKeyStatus()
			self.save_str1=str1
			string=""
			for c in self.getFrame()[game_height-2]:
		                string+=str(c)
		        string+=game1.getSaveKeyStatus()+"\n"
		        f.write(string)
			self.processFrame()
			callback()
			#time.sleep(0.1)
			if self.commandkill:
				break
		
	def startGame(self, callback):
		self.runningThread = threading.Thread(target=self.processor, args=(callback,1))
		self.runningThread.start()
		
	def stopGame(self):
		if self.runningThread==0:
			return
		self.commandkill=1
		self.runningThread=0
	def getKeyStatus(self):
		return str(self.jumpkey)
	def getSaveKeyStatus(self):
		return self.save_str1
game1 = game()


def getPixel(n):
	if n==0:
		return " "
	if n==1:
		return "#"
	if n==2:
		return "*"
	if n==3:
		return ">"
	if n==4:
		return "!"



def renderFrame(game1):
	os.system('clear')
	display_string=""
	for i in range(0, game_height):
		for j in range(0, game_width):
			display_string+=getPixel(game1.getFrame()[i][j])
		display_string+="\r\n"
	string = ""
	print display_string


game1.startGame(lambda:renderFrame(game1))



while(1):
	key=getch()
	if(key=="q"):
		break
	elif(key=="d"):
		game1.pressKeyForward()
	elif(key=="w"):
		game1.pressKeyUp()

game1.stopGame()


f.close()
