'''
Title : Intelligames
Project Members : Agam Agarwal(CS12B1003)
                  Ajay Brahmakshatriya(CS12B1004)
                  Pratik Bhatu(CS12B1010)
'''

import os
import threading
import time
import sys
import termios
import tty
import random

game_height = 10
game_width = 10
high_score = 0

'''
0-blank
1-player
2-obstacle
3-dead
4-coin
'''

class game:
	def __init__(self):
#		random.seed(5)
		self.frame=[[0 for i in range(0, game_height)] for j in range(0, game_width)]
		self.frame[0][0]=1
		self.keys = (0,0)
		self.runningThread=0
		self.commandkill=0
		self.obstacle_counter = 0
		self.pos = 0
		self.score = 0
		self.coin_eaten = 0
		self.gold = 0
		self.game_info=[(game_height,0)]*game_width

	def getPos(self):
		return self.pos
	def getNewLine(self):
		line = [0]*game_height
		r = game_height
		k = 0
		if self.obstacle_counter == 0:
			r = random.randint(0,game_height-1)
			line[r]=2
			line[(r+1)%game_height]=2
			line[(r-1)%game_height]=2
			k = random.randint(0,1)
			if k == 0:
				line[(r-2)%game_height]=4
			else:
				line[(r+2)%game_height]=4

		self.obstacle_counter=(self.obstacle_counter+1)%3
		return (line,r,k)
	def getFrame(self):
		return self.frame
	def setKeysAndProcess(self,keys):
		self.setKeys(keys)
		if self.commandkill==1:
			self.__init__();
		self.processFrame()
	def setKeys(self, keys):
		self.keys=keys
	def processFrame(self):
		self.coin_eaten = 0
		if self.commandkill==1:
			self.keys=(0,0)
			return
		tempFrame = self.frame
		tempFrame = tempFrame[1:]
		self.game_info = self.game_info[1:]
		line = self.getNewLine()
		
		tempFrame.append(line[0])
		self.game_info.append((line[1], line[2]))

		if self.keys==(1,0):
			self.pos-=1
		elif self.keys==(0,1):
			self.pos+=1
		self.pos=self.pos%game_height
		global high_score
		if self.score > high_score:
			high_score = self.score
		if tempFrame[0][self.pos]==4:
			self.coin_eaten=1 
			self.gold+=1
		if tempFrame[0][self.pos]==2:
			tempFrame[0][self.pos]=3
			self.commandkill=1
		else:
			tempFrame[0][self.pos]=1
			self.score+=1

		self.frame=tempFrame
		self.keys=(0,0)
	def getInfo(self):
		return self.game_info
	def processor(self,callback):
		while 1:
			self.processFrame()
			callback()
			time.sleep(0.1)
			if self.commandkill:
				break
	def startGame(self, callback):
                self.runningThread = threading.Thread(target=self.processor, args=(callback,))
                self.runningThread.start()

	def stopGame(self):
		if self.runningThread==0:
                        return
                self.commandkill=1
                self.runningThread=0
	def getScore(self):
		return self.score, self.gold
	def isAlive(self):
		return self.commandkill==0
	def getCoinEaten(self):
		return self.coin_eaten
def getPixel(n):
	return " >#*$"[n]


import pygame
pygame.init()

ship = pygame.image.load("resources/ship.png")
asteroid = pygame.image.load("resources/asteroid.png")
star = pygame.image.load("resources/star.png")
boom = pygame.image.load("resources/boom.png")


def getSprite(n):
	return [0, ship, asteroid, boom, star][n]

def renderFrame(game1):
	screen = pygame.display.set_mode((game_width*32, game_height*32))
	screen.fill(0)
	for i in range(0, game_height):
		for j in range(0, game_width):
			if game1.getFrame()[i][j]!=0:
				screen.blit(getSprite(game1.getFrame()[i][j]),(i*32, j*32))
	pygame.display.flip()
	for i in range(100):	
		for event in pygame.event.get():
			if event.type==pygame.QUIT:
				pygame.quit()
				exit(0)
			if event.type==pygame.KEYDOWN:
				keys=[0,0]
				if event.key==pygame.K_UP:
					keys[0]=1
				elif event.key==pygame.K_DOWN:
					keys[1]=1
				elif event.key==pygame.K_q:
					pygame.quit()
					exit()
				game1.setKeys(tuple(keys))
		pygame.time.wait(1)

if __name__=="__main__":
	game1=game()
	game1.startGame(lambda:renderFrame(game1))


