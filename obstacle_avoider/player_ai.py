'''
Title : Intelligames
Project Members : Agam Agarwal(CS12B1003)
                  Ajay Brahmakshatriya(CS12B1004)
                  Pratik Bhatu(CS12B1010)
'''

from math import radians 
from numpy import *
from RLearning.RLBasic import *
import obstacle_avoider
import time
import json
import itertools
import sys
game_height = obstacle_avoider.game_height
num_rows = 6


def rotate(l,n):
    return l[n:] + l[:n]
def convert(l):
	if 4 in l:
		d = l.index(4)
	else:
		d = obstacle_avoider.game_height
	s=str(l+l)[1:-1]
	if "2, 2, 2" in s:
		p = (s).find("2, 2, 2")/3
	else:
		p = obstacle_avoider.game_height
	return [p,d]

class OA(RLBase):
    def dumpBrain(self, filename):
	js = {"statelist":self.statelist.tolist(), "Q":self.Q}
	json.dump(js, open(filename,"w"))
    def loadBrain(self, filename):
	js = json.load(open(filename))
	self.statelist = array(js["statelist"])
	self.Q = js["Q"]
	
    def disableVideo(self):
	self.video=False
    def enableVideo(self):
	self.video=True
    def init(self):
	self.game = obstacle_avoider.game()   

    def BuildActionList(self):
        return arange(0,4)
        
    def BuildSateList(self):
	states=[[0,0]*num_rows]
	return array(states)

    def GetReward(self, s ):
        # r: the returned reward.
        # f: true if the car reached the goal, otherwise f is false
	r=0		
	if self.game.isAlive():        
		f=False
		r+=1
	else:
		r=-1000
		f=True
	if self.game.getCoinEaten():
		r+=50
	return r,f	

    def DoAction(self, action, x ):
	action = int(action)
	self.game.setKeysAndProcess((action/2, action%2))
	vec = []
	for k in range(0,num_rows):
		info = self.game.getInfo()[k+1]
		if info[0]!=game_height:
			p = (info[0]-self.game.getPos())%game_height
		else:
			p =info[0]
		vec+= [p,info[1]]
	
	if self.video:
		obstacle_avoider.renderFrame(self.game)
		print vec
		time.sleep(0.05)
        return array(vec)


    def GetInitialState(self):
	self.game = obstacle_avoider.game()   
	vec = []
	for k in range(0,num_rows):
		info = self.game.getInfo()[k+1]
		vec+= [(info[0]-self.game.getPos())%game_height,info[1]]
        return array(vec)
    def getStatelistLength(self):
	return self.statelist.shape[0]

def player_ai(maxepisodes):
    CP  = OA(0.3,1.0,0.001)
    CP.disableVideo()
    maxsteps = 1000
    grafica  = False
    if len(sys.argv)>=3:
	    CP.loadBrain(sys.argv[2])
    CP.epsilon = 0.5 
    for j in range(2):    
	    for i in range(maxepisodes):    
    	
	        total_reward,steps  = CP.QLearningEpisode( maxsteps, grafica )
	        
	        print 'Episode: ',i,'  Steps:',steps,'  Reward:',str(total_reward),CP.game.getScore(),' epsilon: ',str(CP.epsilon), 'n_states: ', CP.getStatelistLength()
	    if len(sys.argv)>=2:
		CP.dumpBrain(sys.argv[1])
    	    CP.enableVideo()
	    CP.epsilon = 0
        
      
               
        
if __name__ == '__main__':
	player_ai(10000)
  
