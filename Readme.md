# Introduction #

This is a project which aims at using machine learning techniques to play computer games

# Contributors #

* [Agam Agarwal](mailto:cs12b1003@iith.ac.in)
* [Ajay Brahmakshtriya](mailto:cs12b1004@iith.ac.in)
* [Bhatu Pratik](mailto:cs12b1010@iith.ac.in)
