\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage[breaklinks=true,bookmarks=false,hidelinks]{hyperref}
\usepackage[nameinlink]{cleveref}

\cvprfinalcopy % \item\item\item Uncomment this line for the final submission

\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
%\setcounter{page}{4321}
\begin{document}

%%%%%%%%% TITLE
\title{Intelligames - Using Machine Learning to play games intelligently}

\author{Agam Agarwal\\
CS12B1003\\
IIT Hyderabad\\
{\tt\small cs12b1003@iith.ac.in}
\and
Ajay Brahmakshatriya\\
CS12B1004\\
IIT Hyderabad\\
{\tt\small cs12b1004@ith.ac.in}
\and
Bhatu Pratik\\
CS12B1010\\
IIT Hyderabad\\
{\tt\small cs12b1010@ith.ac.in}
}

\maketitle
%\thispagestyle{empty}

\begin{abstract} 

In this paper we aim to make an AI agent which would autonomously play games.
Programming an agent to play a game is not that difficult and can be done
utilizing the physics of the game. The problem we are trying to solve here is to
use an advanced and more intelligent AI technique which is capable of solving a
specific problem by itself without domain knowledge of the game. To achieve this
we use Reinforcement Learning to evolve an intelligent agent to play Flappy bird
and Space Invaders. Our approach can be generalized to many other games.

\end{abstract}

\section{Introduction} 

Computer games provide a complex benchmark to test learning algorithms. In this
paper we use such a learning algorithm to develop an artificial agent capable of
playing two games: Space Invaders \cref{fig:space} and Flappy Bird \cref{fig:flappy}.
\begin{enumerate}
    \item \textbf{Space Invaders:} 
        An obstacle avoider game.
        The player is a spaceship which moves forward at a constant velocity.
        The aim of the game is to avoid incoming asteroids and collect coins.
        The game also allows wrap around at limits.

        Controls: Move Left, Move Right, Do Nothing

    \item \textbf{Flappy Bird:} 
        The game is a side-scroller where the player controls a bird, attempting
        to fly between rows of green pipes without coming into contact with
        them.

        Controls: Jump, Do Nothing

\end{enumerate}

The challenge of this problem is not only making an agent for these games that
knows how to act in different situations but also to translate the game state
into a format that the agent can process and act upon.

The games used in this paper were implemented in python by us. We have created
an interface for both the games that allows any AI agent to control the game.
The agent is given access to a simple state representation of the game as input
and the agent should be able to determine which action - left, right for Space
Invaders and jump for Flappy Bird, to take in a given situation. Initially we
had used an Artificial Neural Network solution, but we discarded it due to poor
performance. We now use the Q-learning algorithm.

\begin{figure}
    \centering
    \begin{subfigure}[t]{0.3\textwidth}
        \includegraphics[width=\textwidth]{images/spaceInvader.png}
        \caption{Space Invaders}
        \label{fig:space}
    \end{subfigure}
    \begin{subfigure}[t]{0.3\textwidth}
        \includegraphics[width=\textwidth]{images/flappyBird.png}
        \caption{Flappy Bird}
        \label{fig:flappy}
    \end{subfigure}
    \caption{Target games for AI.}
\end{figure}


\section{Methodology}
\subsection{Extracting Game Information}
In the system we implemented, the agent is given frames as input.
It analyzes the given frame and searches it for all the game objects.
The first method we used for this search involved simply matching the image templates of all the game objects with the current frame.
This approach works poorly for our use case.
This is because mostly the sprites used in games have a lot transparent regions, i.e., regions with $alpha=0$.
Vanilla template matching performs poorly with such transparent regions because it attempts to find the maximum score after cross-correlation and regions with $alpha=0$ match poorly with the frame where pixel is opaque.
The background interferes with the the template matching process.

A technique to solve this issue is to use \emph{masks} while matching templates.
This mask should ensure that only the opaque regions of the image are used while matching.
Another method is to pre-process the template and the frame in order to eliminate the background interference.
We used the latter technique.
The pre-processing involves using the Canny edge detector \cite{canny1986computational} to detect edges in the template as well as the frame.
This gives low response for flat or transparent regions while giving high responses for the edge pixels.
The next step involves matching the modified template with the modified frame.
As the edge detection decreases the interference due to the background, template matching now gives better results and the game objects are matched in the frame at their approximately correct positions.

\subsection{State representation}
The information about the locations of the objects as detected above is now converted to game state.
The state is a compact representation of the locations of the objects present in the frame.
The method of conversion of locations to state is game dependent.

In our implementation, the game state for the space invaders game was represented by the positions of the obstacles and coins in the next 6 columns.
The positions were given relative to the player so as to further decrease the number of possible states as the position of the player itself doesn't need to encoded in the state now.

For the Flappy Bird game, the game state comprises of the height of the closest pipe with respect to the bird.
This information turns out to be sufficient for the bot to learn how to play the game.

We tried many different ways of representing the data before finalizing the state representation given above.
We notice that the amount data given as state and also the way in which it is encoded matters a lot.
As the size of the state representation increases, the amount of time taken for convergence increases.
On the other hand, if the state contains too little information, the bot may fail to converge.
Thus, it is very important to determine a compact way of representing the game state.

\subsection{Q-Learning}

Q-learning essentially learns a function that gives us the expected utility of
taking a given action in a given state. It then follows the optimum policy to
achieve maximum utility i.e. given a state it always selects the action with the
highest value. The model consists of an agent, states $S$ and a set of actions
$A$. Executing an action in a specific state provides the agent with a reward.
The goal of the agent is to maximize its total reward. It does this by learning
which action is optimal for each state.

The action that is optimal for each state is the action that has the highest
long-term reward. This reward is a weighted sum of the expectation values of the
rewards of all future steps starting from the current state. The weight for a
step from a state $\Delta t$ steps in future is equal to $\gamma^{\Delta t}$.
Here $\gamma \in [0,1]$ is the discount factor which decides importance of
sooner or later rewards. \\

Basically for each $s \in S$ we maintain action values (Q-values) for each
action $a \in A$. We store these values in a `Q-table'. On taking an action the
agent goes to another state and receives some reward. We then update the Q-value
based on this reward, old state and new state information as follows:

\begin{equation*}
\begin{split}
Q_{t+1}(s_t,a_t) = Q_{t}(s_t,a_t) + \alpha_t(s_t,a_t) * \Bigg( R_{t+1} + \gamma 
\\ * \max_{a} Q_t(s_{t+1},a) - Q_t(s_t,a_t) \Bigg)
\end{split}
\end{equation*}

where $Q_{t}(s_t,a_t)$ is the old Q-value, $\alpha_t(s_t,a_t)$ is the learning
rate, $R_{t+1}$ is the reward, $\gamma$ is the discount factor and $\max_{a}
Q_t(s_{t+1},a)$ is the estimate of optimal future value.

The learning rate $\alpha$ determines to what extent the newly acquired
information will override the old information. A factor of 0 will make the agent
not learn anything, while a factor of 1 would make the agent consider only the
most recent information.The discount factor $\gamma$ determines the importance
of future rewards. A factor of 0 will make the agent short-sighted by only
considering current rewards, while a factor approaching 1 will make it strive
for a long-term high reward.\\

For this algorithm we used the library provided by \cite{qlearning}. This
implementation, however, has a limitation. It requires you to build a state list
of all possible states before hand. It is easy to see that for any complex
representation this can blow up in space consumption. Say you are using 32 bits
to encode your game state. We will have to generate an array of size $2^{32} *
10$. The memory usage would be equal to around 40 Gb! To solve this problem
we modify the implementation. Instead of building the state list at the start
itself we use an online approach. Every time we get a game state, we check the
state list table for it and if the table does not contain this state we add the
state to it. This does not blow up in space because in practise the states
encountered by the agent are very limited. For the Space Invader game we use 12
integers to represent the game state where each integer can take value from 0 to
11. So we have $11^{12} (3138428376721)$ possible states. However during
training we see that we only encounter 1262 states. This happens because most of
the $11^{12}$ states do not correspond to actual possible game states. In the
Space Invader game we can see that obstacles/stars only come every alternate
column but the they would also come in each column if we consider all states.
The scenario of an entire column of asteroids can also come up in the calculated
states but it is not actually possible in the game.

\section{Results}

The aim of the system is to learn to play the game and try to attain the maximum possible score.
So, we evaluate the system based the score it achieves.
Both the games we used in our project are never-ending games.

At every frame, the bot is given the frame of the game.
The bot uses information from the frame to take an action.
Based on the current state of the game and the action taken, the bot is rewarded (positively or negatively).
The reward adds up to the score of the game.
We evaluated our system on basis of how well does it learn to take actions which give better rewards and hence attain a higher score.

\subsection{Space Invaders}

\subsubsection{Version 1}

\begin{itemize}
\item \textbf{Game} - 1 obstacle per vertical line
\item \textbf{State} - 1 bit - True if the block in front of the player is an obstacle, false otherwise.
\item \textbf{Number of States} - $2^1$
\item \textbf{Result} - The agent learned to avoid obstacles in around 10 iterations. However, the agent moved only in a single direction (upward or downwards).
\end{itemize}

\subsubsection{Version 2}

\begin{itemize}
\item \textbf{Game} - 2 obstacles per vertical line
\item \textbf{State} - 3 bits - representing the blocks one step right, one step diagonally up, one step diagonally down.
\item \textbf{Number of States} - $2^3$
\item \textbf{Result} - The agent learned to avoid obstacles in around 10 iterations. This time, the agent moved in the appropriate direction in order to avoid the obstacle.
\end{itemize}

\subsubsection{Version 3}

\begin{itemize}
\item \textbf{Game} - 3 obstacles per vertical line, then a line without obstacles
\item \textbf{State} - 3 bits - representing the blocks one step right, one step diagonally up, one step diagonally down.
\item \textbf{Number of States} - $2^3$
\item \textbf{Result} - The agent failed to learn to avoid the obstacles. This was because the provided information was too less to take any decisions.
\end{itemize}

\subsubsection{Version 4}

\begin{itemize}
\item \textbf{Game} - 3 obstacles per vertical line, then a line without obstacles
\item \textbf{State} - 6 bits - representing the corresponding 3 blocks for the next two lines.
\item \textbf{Number of States} - $2^6$
\item \textbf{Result} - The agent learned to avoid the obstacles in around 100 iterations. The agent also learned to start moving away even when the next obstacle is two blocks away. The agent moves up or down based on which end is nearer. If both ends are at an equal distance, the agent always moves in a single direction (up or down).
\end{itemize}

\subsubsection{Version 5}

\begin{itemize}
\item \textbf{Game} - 3 obstacles per vertical line, then a line without obstacles. A coin may be present either above or below the obstacle.
\item \textbf{State} - Information about the next two rows. For each row, it contains the location of the obstacle, and the position of the coin with respect to the obstacle.
\item \textbf{Number of States} - $(11 * 2) ^ 2$
\item \textbf{Result} - The agent learned to avoid obstacles in around 500 iterations. It moves towards the nearer end of the obstacle. If both ends are at an equal distance, the agent moves towards the direction of the coin in order to gain higher reward.
\end{itemize}

\subsection{Flappy Bird}
For testing our system with the Flappy Bird game, we passed the current frame of the game to our system.
As stated above, the system detected the locations of different objects in the frame using computer vision techniques and these locations acted as the states of the game.
We tested the results after different number of iterations of training.
Each iteration allowed a maximum of 10000 steps.

\subsubsection{0 Iterations}
Without any training the bot just jumps and keeps going up until it crosses the height limit and dies.

\subsubsection{5 Iterations}
The bot jumps and starts to go up.
However, the bot waits for sometime before taking subsequent jumps.
Ultimately, it crosses the height limit and dies.

\subsubsection{15 Iterations}
The bot learns to jump appropriately and moves forward properly.
However, the bot has not yet learned to go through the gaps in pipes.
It collides with the very first pipe and dies.

\subsubsection{20 Iterations}
The bot learns to go towards the gaps and jumps accordingly.
It learns to enter the gaps.
However, it collides with the internal wall of the pipes and dies.

\subsubsection{25 Iterations}
The bot is now completely trained and learns to play the game properly.
It passes through all the gaps and can now survive the game forever.

\section{Conclusion}

In this paper we built autonomous agents using a Reinforcement Learning
technique namely Q-Learning. The agent is able to play Flappy Bird and Space
Invaders flawlessly i.e., it never dies. We also saw that attention needs to be
paid on how we feed game state information to the algorithm. One should aim for
a concise representation but it should also contain enough information to play
the game.

\nocite{*}
{\small
\bibliographystyle{ieee}
\bibliography{references}
}

\end{document}
