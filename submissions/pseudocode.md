Intelligames - Using Machine Learning Intelligently to play Games
=================================================================

Our project aims at developing a bot which interfaces with a computer game and learns to play it. The project is split in two phases:

 1. Extracting game information: For example, player position, relative position of enemies, obstacles, reward coins, etc.
 
 2. Machine Learning: Neural Networks and Reinforcement Learning.

Extracting Game Information
---------------------------

Here we use template matching. We will use the sprites(images) of the game.For example if we consider the Mario game we use sprites for the character *mario*, pipes, enemies, blocks and coins. 

Pseudo Code for template matching:

```
FUNCTION GetStateFromFrame(frame):
state = *empty*
FOREACH sprite
	locations = TemplateMatching(frame, sprite)
	ADD locations, type_of_sprite TO state
RETURN state
```
Mario has been implemented in **Java** for the *MarioAI* competition and we are using their interface. (www.code.google.com/p/marioai)


Machine Learning: Neural Network
--------------------------------

Here the input for the network is the current game state and the output nodes are the possible moves to take (Move Forward, Jump, Shoot). So if the output is `[1, 1, 0]`, the move to be made is Move Forward AND Jump. We generate training data by making random moves in the game. If the move results in increase in reward we add it to the data set with previous move as label(desired move).

```
FUNCTION GetReward(action):
	DO action			// Change current_state
	IF Player.IsDead()
		RETURN -1000	// Negative Reward
	ELSE
		RETURN IncreaseInScore()
	END IF

FUNCTION GetTrainingData(number_of_data_points):
	training_data = []
	FOR I = 0 to number_of_data_points
		old_state = current_state
	action = RandomAction()
		IF GetReward(action) > 0
			ADD old_state, action TO training_data
		END IF
	END FOR
	RETURN training_data
```

We use the *pybrain* library (www.pybrain.org) implementation of neural network. The pseudo code for training a network is given below.

```
FUNCTION TrainNeuralNetwork():
	network = NeuralNetwork(node_count_in_layers = [length of state, 100, 100, 3])
	FOR input, label IN training_data:
		output = ForwardPropagation(input)
		error_in_last_layer = label - output
		BackPropagation(error_in_last_layer) 	// Update weights
	END FOR
	RETURN network
```

Machine Learning : Reinforcement Learning
-----------------------------------------

Reinforcement learning is a machine learning technique inspired by behaviorist psychology. In this technique, a software agent takes *actions* based on certain *observations* and attempts to maximize *reward*.

It is formulated as a Markov Decision Process. We represent the game using states. Given current state(observation), it tries to learn  what action it should take to maximize reward.
This is an online method, so we do not require to generate traditional training data. We let the machine make a move in the game and we return the reward for that move.

We use Q-Learning which maintains a Q-table for probabilities of actions to take for each state. So if the no. of states is `m` and no. of actions is `n` the size of table is `m x n`. 

Pseudo-code
------------

Variables:

* learning rate - `alpha`
* discount factor - `gamma`
* probability of a random action selection - `epsilon`

```
FUNCTION BuildQTable():
	Q = CREATE Table(size = (no_of_states,no_of_actions))
	INITIALIZE ALL IN Q TO 0.0
	RETURN Q

// Given state we choose best action based on highest probability.
FUNCTION GetBestAction(state):
	action = ARGMAX(Q[state])
	RETURN action

// We also take random actions instead of best action always to escape local minima. 
// This is called Epsilon-greedy strategy
FUNCTION EpsilonGreedyAction(state):
	IF Random() > epsilon
		action = GetBestAction(state)
	ELSE
		action = RandomInt(min=0, max=no_of_actions-1)
	END IF

//Given state, action, reward, new_state update Q-table using Q-learning algorithm
FUNCTION UpdateQ(s=old_state, a=action, r=reward, sp=new_state)
	probability_of_best_action = max(Q[sp])
	Q[s][a] = Q[s][a] + alpha*(r + gamma*probability_of_best_action - Q[s][a])

//This is the main function.
FUNCTION DoQLearning():
	state = GetInitialState()
	FOR i = 0, no_of_epochs
		action = EpsilonGreedyAction(state)
		new_state = DoAction(action,state)
		reward = GetReward(new_state)
		UpdateQ(state,action,reward,new_state)
		state = new_state
	END FOR

//Return the reward after the last action using the state
//This is a general function and will fit the definition of any game
FUNCTION GetReward(state)
	USE state TO CALCULATE reward
	IF end of game is reached
		flag = true
	ELSE
	flag = false
	END IF
	RETURN reward, flag

//Perform the given action and get the next state
//This is a general function and will fit the definition of any game
FUNCTION DoAction(action, lastState)
	newState = lastState
	APPLY action ON newState
	RETURN newState
```

Progress
----------
We created a simple game of obstacle avoider and used it to test our code. In this game the character moves up and down with the aim to avoid obstacles and collect coins for achieving maximum score. The game allows vertical wrap around across the screen.

In the figure below, the following characters are used to represent different entities.

| Character | Entity   |
| --------- | -------- |
| `>`       | Player   |
| `#`       | Obstacle |
| `$`       | Coin     |


```
----------˥
#  $  #  #|
#  #  $  $|
#  #      |
   #      |
          |
>         |
          |
          |
      #  #|
$     #  #|
----------˩
```
We worked with grid size `10 x 10`

The following is the implementation of the `GetReward()` and `DoAction()` for the obstacle avoider game

```
//Return the reward after the last action using the state
FUNCTION GetReward(lastState)
	reward = 0
	IF player is alive
		flag = true
		reward = reward + 1
	ELSE
		flag = false
		reward = reward - 1000
	END IF
	IF coin taken
		reward = reward + 10
	END IF
	RETURN reward, flag

//Perform the given action and get the next state
FUNCTION DoAction(action, lastState)
	newState = lastState
	UPDATE newState by a frame
	IF action == 0
		move up and UPDATE newState
	ELSE IF action == 1
		move down and UPDATE newState
	END IF
	RETURN newState
```


Algorithm Testing
-----------------

The main challenge in Q-learning is how we represent the game in the form of states. Simply giving a frame can lead to expontial blow-up in the number of states. This approach isn't scalable. We need to give a minimalistic representation of the game as the game state.

We increase the game complexity iteratively and discuss the state representation and results.

### Version 1
* Game - 1 obstacle per vertical line
* State - 1 bit - True if the block in front of the player is an obstacle, false otherwise.
* Number of States - 2^1
* Result - The agent learned to avoid obstacles in around 10 iterations. However, the agent moved only in a single direction (upward or downwards).

### Version 2
* Game - 2 obstacles per vertical line
* State - 3 bits - representing the blocks one step right, one step diagonally up, one step diagonally down.
* Number of States - 2^3
* Result - The agent learned to avoid obstacles in around 10 iterations. This time, the agent moved in the appropriate direction in order to avoid the obstacle.

### Version 3
* Game - 3 obstacles per vertical line, then a line without obstacles
* State - 3 bits - representing the blocks one step right, one step diagonally up, one step diagonally down.
* Number of States - 2^3
* Result - The agent failed to learn to avoid the obstacles. This was because the provided information was too less to take any decisions.

### Version 4
* Game - 3 obstacles per vertical line, then a line without obstacles
* State - 6 bits - representing the corresponding 3 blocks for the next two lines.
* Number of States - 2^6
* Result - The agent learned to avoid the obstacles in around 100 iterations. The agent also learned to start moving away even when the next obstacle is two blocks away. The agent moves up or down based on which end is nearer. If both ends are at an equal distance, the agent always moves in a single direction (up or down).

### Version 5
* Game - 3 obstacles per vertical line, then a line without obstacles. A coin may be present either above or below the obstacle.
* State - Information about the next two rows. For each row, it contains the location of the obstacle, and the position of the coin with respect to the obstacle.
* Number of States - (11 * 2) ^ 2
* Result - The agent learned to avoid obstacles in around 500 iterations. It moves towards the nearer end of the obstacle. If both ends are at an equal distance, the agent moves towards the direction of the coin in order to gain higher reward.

Challenges
----------
As the game complexity increases, the number of states required to represent the game also increases. Therefore, for complex games like Mario, it would be challenging to represent the game state.

For example, if we use 32 bits to encode states, the number of possible states are 2^32 (4,294,967,296). In our experiments, generating such a large state list resulted in termination of the program due to excess use of memory.

However, an important observation is that not all of the 2^32 states will occur in the game, only a subset will. We aim to take this fact into account and generate a smaller state list.

Team
----
* Agam Agarwal (CS12B1003)
* Ajay Brahmakshatriya (CS12B1004)
* Bhatu Pratik (CS12B1010)
