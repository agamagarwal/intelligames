'''
Title : Intelligames
Project Members : Agam Agarwal(CS12B1003)
                  Ajay Brahmakshatriya(CS12B1004)
                  Pratik Bhatu(CS12B1010)
'''

import pygame
from pygame.locals import *
import random

pygame.init()
width, height = 284*2, 512
pipe_speed = 1
pipe_gap = 5
pipe_interval = 300
bird_fall_speed = 1.5
pygame.display.set_caption("FLAPPY BIRD")
bird = pygame.image.load("resources/bird_wing_up.png")
pygame.display.set_icon(bird)
background = pygame.image.load("resources/background.jpg")
pipe_body = pygame.image.load("resources/pipe_body.png")
pipe_end = pygame.image.load("resources/pipe_end.png")

bird_x = 80
bird_jump_distance = 5
bird_jump_amount = 150

MODE_VISUAL = 0
MODE_FAST = 1
font = pygame.font.Font("resources/font.ttf", 50)

def check_collision(r1, r2):
	if r1[0]>r2[2] or r2[0]>r1[2] or r1[1]>r2[3] or r2[1]>r1[3]:
		return False
	return True

class game:
	def set_mode(self, mode):
		self.mode = mode

	def __init__(self, mode):
		#random.seed(5)
		self.mode = mode
		if mode == MODE_VISUAL:
			self.screen = pygame.display.set_mode((width, height))
		elif mode == MODE_FAST:
			self.screen = pygame.Surface((width, height))

		self.pipes = [(568,4)]

		self.pipe_counter=0
		self.bird_position = 256

		self.jump_distance = 0

		self.game_start = False
		self.game_end = False

		self.score = 0
	
	def process_frame(self):
		if self.mode == MODE_VISUAL:
			self.screen.fill(0)
			self.screen.blit(background, (0,0))
			self.screen.blit(background, (284,0))
			for j in range(len(self.pipes)):
				pipe = self.pipes[j]
				for i in range(pipe[1]):
					self.screen.blit(pipe_body, (pipe[0], i*32))
				self.screen.blit(pipe_end, (pipe[0], pipe[1]*32))
				self.screen.blit(pipe_end, (pipe[0], (pipe[1]+1+pipe_gap)*32))
				for i in range((pipe[1]+2+pipe_gap),16):
					self.screen.blit(pipe_body, (pipe[0], i*32))
					
		if self.game_start and self.game_end==False:
			for j in range(len(self.pipes)):
				pipe = self.pipes[j]
				self.pipes[j] = (pipe[0]-pipe_speed, pipe[1])
			if len(self.pipes)>0 and self.pipes[0][0]+80==bird_x:
				self.score+=1	
			if len(self.pipes)>0 and self.pipes[0][0]< -80:
				self.pipes = self.pipes[1:]
			self.pipe_counter +=1

			if (self.pipe_counter >= pipe_interval/pipe_speed):
				self.pipe_counter=0
				self.pipes = self.pipes+[(568, random.randint(3,8))]

			self.bird_position+=bird_fall_speed
			if self.jump_distance !=0 :
				self.bird_position-=bird_jump_distance
				self.jump_distance -=1
			if self.bird_position > height or self.bird_position<-32:
				self.game_end = True
		
			if len(self.pipes)>0:
				if check_collision((bird_x, self.bird_position, bird_x+32, self.bird_position+32), (self.pipes[0][0]+1, 0, self.pipes[0][0]+1+80, (self.pipes[0][1]+1)*32)):
					self.game_end = True
				if check_collision((bird_x, self.bird_position, bird_x+32, self.bird_position+32), (self.pipes[0][0]+1, (self.pipes[0][1]+1+pipe_gap)*32, self.pipes[0][0]+1+80, height+1)):
					self.game_end = True
		if self.mode == MODE_VISUAL:
			self.screen.blit(bird, (bird_x, self.bird_position))
			score_board = font.render(str(self.score), 1, (150,50,50))

			self.screen.blit(score_board, (10,10))
		if self.mode == MODE_VISUAL:
			for event in pygame.event.get():
				if event.type==pygame.QUIT:
					pygame.quit()
					exit(0)
				elif event.type==pygame.KEYDOWN:
					if event.key==pygame.K_SPACE:
						self.press_jump()
					if event.key==pygame.K_r:
						if self.game_end == True:
							self.__init__(self.mode)

		if self.jump_distance == 0 or self.game_end:
			return False
		return True
	def render(self):
		if self.mode == MODE_VISUAL:
			pygame.display.flip()	
	def isAlive(self):
		return self.game_end==False
	def start(self):
		self.game_start = True
	def start_render(self):
		if self.mode != MODE_VISUAL:
			return
		while(1):
			self.process_frame()
			self.render()
	def get_score(self):
		return self.score
	def get_buffer(self):
		return self.screen
	
	def press_jump(self):
		if self.game_start == False:
			self.game_start = True
		else:
			if self.jump_distance == 0:
				self.jump_distance = bird_jump_amount/bird_jump_distance
	def get_bird(self):
		return self.bird_position
	def get_pipes(self):
		return self.pipes			

if __name__=="__main__":
	game1 = game(MODE_VISUAL)
	game1.start_render()
	
